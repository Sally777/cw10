CREATE SCHEMA `news_app` DEFAULT CHARACTER SET utf8 ;

USE `news_app`;

CREATE TABLE .`news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL
);

INSERT INTO `news` (`id`, `title`, `content`)
VALUES 
   (1, 'Парламент Британии снова отверг соглашение с ЕС, стране грозит "обвальный брексит"', 'Парламент Британии в пятницу в третий раз отверг соглашение об условиях выхода Британии из Европейского союза. Перед Британией в полный рост встала перспектива "жесткого брексита" - выхода из ЕС 12 апреля без урегулированных отношений.'),
   (2, 'No criminal charges planned in case where children were pushed, dragged at shelter"', 'Video footage obtained by The Republic late last year showed three separate incidents of children being apparently maltreated by staff at a Southwest Key shelter in Youngtown, near Phoenix.')