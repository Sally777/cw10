import React, {Component, Fragment} from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {fetchNews} from "../store/actions/newsActions";
import {Button, Card, CardBody} from "reactstrap";


class News extends Component {
    componentDidMount() {
        this.props.onFetchNews();
    }

    render() {
        return (
            <Fragment>
                <h2>
                    Posts
                    <Link to="/news/new">
                        <Button
                            color="primary"
                            className="float-right"
                        >
                            Add new post +
                        </Button>
                    </Link>
                </h2>
                {this.props.news.map(news => (
                    <Card key={news.id} style={{width: "400px", margin: "10px 0"}}>
                        <CardBody>
                            <strong>{news.title}</strong>

                            <p>     </p>
                            <Link to={'/news/' + news.id}>Read full post >> </Link>
                        </CardBody>
                    </Card>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news.news
});

const mapDispatchToProps = dispatch => ({
    onFetchNews: () => dispatch(fetchNews())
});

export default connect(mapStateToProps, mapDispatchToProps)(News);