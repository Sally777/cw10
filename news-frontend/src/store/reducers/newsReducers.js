import {FETCH_NEWS_SUCCESS} from "../actions/newsActions";

const initialState = {
    news: []
};

const newsReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news};
        default:
            return state;
    }
};

export default newsReducers;