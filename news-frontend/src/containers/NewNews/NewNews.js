import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import NewsForm from "../../components/NewsForm/NewsForm";
import {createNews} from "../../store/actions/newsActions";


class NewNews extends Component {
    createNews = newsData => {
        this.props.onNewsCreated(newsData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>Add new post</h2>
                <NewsForm onSubmit={this.createNews}/>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onNewsCreated: newsData => dispatch(createNews(newsData))
});

export default connect(null, mapDispatchToProps)(NewNews);