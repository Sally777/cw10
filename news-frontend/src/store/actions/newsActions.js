import axios from '../../axios-api';

export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';

export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});


export const fetchNews = () => {
    return dispatch => {
        return axios.get('/news').then(
            response => dispatch(fetchNewsSuccess(response.data))
        );
    };
};

export const createNews = newsData => {
    return dispatch => {
        return axios.post('/news', newsData).then(
            () => dispatch(createNewsSuccess())
        );
    };
};