const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {
    res.send(db.getItems())
});

router.get('/:id', (req, res) => {
    res.send('')
});

router.post('/', (req,res) => {
    if(req.body.title &&  req.body.content) {
        db.addItem(req.body);
        res.status(201).send({message: 'OK'});
    } else {
        res.status(400).send({message: 'Enter news'});
    }

});

module.exports = router;