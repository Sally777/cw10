import React from 'react';
import {Nav, Navbar, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const Toolbar = () => {
    return (
        <Navbar color="light" light expand="md">
            <Nav className="mr-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact style={{fontSize: "35px"}}><strong>News</strong></NavLink>
                </NavItem>
            </Nav>
        </Navbar>
    );
};

export default Toolbar;