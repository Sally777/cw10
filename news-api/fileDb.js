const fs = require('fs');
const nanoid = require('nanoid');
const filename = '.db.json';

let initialData = {
    news: []
};

let data = {};

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getItems() {
        return data;
    },
    addItem(item) {
        item.date = new Date().toISOString();
        item.id = nanoid();
        data.push(item);
        this.save();
    },
    save() {
        fs.writeFileSync(filename, JSON.stringify(data, null,2));
    }
};

