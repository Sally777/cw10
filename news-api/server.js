const express = require('express');
const mysql  = require('mysql');
const news = require('./app/news');
const cors = require('cors');


const app = express();
app.use(express.json());
app.use(cors());



var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'user',
    password : '1993Aliya',
    database: 'news_app'
});

const port = 8000;

app.use('/news', news);

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
    app.listen(port, () => {
        console.log(`Server started on ${port} port `);
});

});