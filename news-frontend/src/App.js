import React, { Component, Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import News from "./containers/News";
import NewNews from "./containers/NewNews/NewNews";
import Toolbar from "./components/UI/Toolbar";
import {Container} from "reactstrap";

import './App.css';

class App extends Component {
  render() {
    return (
        <Fragment>
          <header>
            <Toolbar/>
          </header>
          <Container style={{marginTop: '20px'}}>
            <Switch>
              <Route path="/" exact component={News}/>
              <Route path="/news/new" exact component={NewNews}/>
            </Switch>
          </Container>
        </Fragment>
    );
  }
}

export default App;
